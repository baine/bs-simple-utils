let rec somesHelper = (accumulated, items) =>
  switch (items) {
  | [] => accumulated
  | [h, ...t] =>
    somesHelper(
      switch (h) {
      | Some(x) => [x, ...accumulated]
      | None => accumulated
      },
      t,
    )
  };

let somes = items => somesHelper([], items);

let rec j = (accumulated, sep, items) =>
  switch (items) {
  | [] => accumulated
  | [head, ...tail] => j(accumulated ++ sep ++ head, sep, tail)
  };

let join = (sep, xs) =>
  switch (xs) {
  | [] => ""
  | [head, ...tail] => j(head, sep, tail)
  };
